// function getRes(response) {
//   var newRess = {
//     status: response.status,
//     data: response.data,
//     error: "",
//     message: response.data.message,
//     token: ""
//   };

//   if (response.status == 200) {
//     newRess.status = response.data.status;
//     newRess.data = response.data;
//     newRess.error = response.data.message;
//     newRess.message = response.data.message;
//     newRess.token = response.data.data.token;
//   }

//   return newRess;
// }

export default class {
  static PostData(BaseApi, url, objData, onSuccess, onError) {
    return BaseApi.post(url, objData)
      .then(data => onSuccess(data))
      .catch(err => onError(err));
  }
  static GetData(BaseApi, url, objData, onSuccess, onError) {
    return BaseApi.get(url, objData)
      .then(data => onSuccess(data))
      .catch(err => onError(err));
  }
  static DeleteData(BaseApi, url, contentType, onSuccess, onError) {
    return BaseApi.delete(url, contentType)
      .then(data => onSuccess(data))
      .catch(err => onError(err));
  }
  static UpdateData(BaseApi, url, objData, onSuccess, onError) {
    return BaseApi.update(url, objData)
      .then(data => onSuccess(data))
      .catch(err => onError(err));
  }
  static PatchData(BaseApi, url, objData, onSuccess, onError) {
    return BaseApi.patch(url, objData)
      .then(data => onSuccess(data))
      .catch(err => onError(err));
  }
  static PutData(BaseApi, url, contentType, onSuccess, onError) {
    return BaseApi.put(url, contentType)
      .then(data => onSuccess(data))
      .catch(err => onError(err));
  }
}
