import { createApp } from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import JwtService from "@/core/jwt.service";

/**
 * Service to call HTTP request via Axios
 */
const app = createApp()
const ApiService = {
  init() {
    app.use(VueAxios, axios);
    app.axios.defaults.baseURL = "https://dev.api.universal-iot.com/test-interview/v-1/frontend/";
  },

  /**
   * Set the default HTTP request headers
   */
  setHeader() {
    app.axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${JwtService.getToken()}`;
  },

  query(resource, params) {
    return app.axios.get(resource, params).catch(error => {
      // console.log(error);
      throw new Error(`[KT] ApiService ${error}`);
    });
  },

  /**
   * Send the GET HTTP request
   * @param resource
   * @param slug
   * @returns {*}
   */
  get(resource, params) {
    return app.axios.get(`${resource}`, params);
  },

  /**
   * Set the POST HTTP request
   * @param resource
   * @param params
   * @returns {*}
   */
  post(resource, params) {
    return app.axios.post(`${resource}`, params );
  },

  /**
   * Send the UPDATE HTTP request
   * @param resource
   * @param slug
   * @param params
   * @returns {IDBRequest<IDBValidKey> | Promise<void>}
   */
  update(resource, params) {
    return app.axios.put(`${resource}`, params );
  },

  patch(resource, params) {
    return app.axios.patch(`${resource}`, params );
  },

  /**
   * Send the DELETE HTTP request
   * @param resource
   * @returns {*}
   */
  delete(resource, contentType) {
    return app.axios.delete(`${resource}`, contentType, {
      headers: {
        "Content-Type": contentType,
        Accept: "*/*",
        "Access-Control-Allow-Origin": "*"
      }
    });
  }
};

export default ApiService;
