import ApiService from "./api.services";
import JwtService from "./jwt.service";

import Services from "./Services";
import Swal from "sweetalert2";

// action types
export const VERIFY_AUTH = "verifyAuth";
export const LOGIN = "login";
export const LOGOUT = "logout";
export const REGISTER = "register";
export const UPDATE_PASSWORD = "updatePassword";
export const UPDATE_USER = "updateUser";

// mutation types
export const PURGE_AUTH = "logOut";
export const SET_AUTH = "setUser";
export const SET_PASSWORD = "setPassword";
export const SET_ERROR = "setError";

const state = {
  errors: null,
  user: {},
  isAuthenticated: !!JwtService.getToken()
};

const getters = {
  currentUser(state) {
    return state.user;
  },
  isAuthenticated(state) {
    return state.isAuthenticated;
  }
};

const actions = {
  [LOGIN](context, credentials) {
    return new Promise(resolve => {
      var mydata = {
        email: credentials.email,
        password: credentials.password
      };

      let contentType = `application/x-www-form-urlencoded`;

      const qs = require("qs");

      Services.PostData(
        ApiService,
        "authentication/login",
        qs.stringify(mydata),
        contentType,
        response => {
          resolve(response);
          if (response.data != null) {
            context.commit(SET_AUTH, response.data);
          } else {
            context.commit(SET_ERROR, []);
            Swal.fire({
              title: "",
              text: "Gagal Login",
              icon: "error",
              confirmButtonClass: "btn btn-secondary",
              heightAuto: true,
              timer: 1500
            });
          }
        },
        err => {
          Swal.fire({
            title: "Gagal Login",
            text: err.response.data.message,
            icon: "error",
            confirmButtonClass: "btn btn-secondary",
            heightAuto: true,
            timer: 1500
          });
          context.commit(SET_ERROR, err);
        }
      );
    });
  },
  [LOGOUT](context) {
    context.commit(PURGE_AUTH);
  },
  [REGISTER](context, credentials) {
    return new Promise(resolve => {
      var mydata = {
        email: credentials.email,
        username: credentials.username,
        password: credentials.password
      };

      let contentType = `application/x-www-form-urlencoded`;

      const qs = require("qs");

      Services.PostData(
        ApiService,
        "authentication/register",
        qs.stringify(mydata),
        contentType,
        response => {
          resolve(response);
          if (response.data != null) {
            context.commit(SET_AUTH, response.data);
          } else {
            context.commit(SET_ERROR, []);
          }
        },
        err => {
          context.commit(SET_ERROR, err);
        }
      );
    });
  },
  [VERIFY_AUTH](context) {
    if (JwtService.getToken()) {
      ApiService.setHeader();
    } else {
      context.commit(PURGE_AUTH);
    }
  },
  [UPDATE_PASSWORD](context, payload) {
    const password = payload;

    return ApiService.put("password", password).then(({ data }) => {
      context.commit(SET_PASSWORD, data);
      return data;
    });
  },
  [UPDATE_USER](context, credentials) {
    return new Promise(resolve => {
      var mydata = {
        name: credentials.name
      };

      var uid = credentials.uid;

      let contentType = `application/x-www-form-urlencoded`;

      const qs = require("qs");

      Services.UpdateData(
        ApiService,
        `account/profile`,
        qs.stringify(mydata),
        contentType,
        response => {
          resolve(response);
          if (response.data != null) {
            Swal.fire({
              title: "",
              text: "Berhasil Update",
              icon: "success",
              confirmButtonClass: "btn btn-secondary",
              heightAuto: true,
              timer: 1500
            });
          } else {
            context.commit(SET_ERROR, []);
            Swal.fire({
              title: "",
              text: "Gagal Login",
              icon: "error",
              confirmButtonClass: "btn btn-secondary",
              heightAuto: true,
              timer: 1500
            });
          }
        },
        err => {
          context.commit(SET_ERROR, err);
        }
      );
    });
  }
};

const mutations = {
  [SET_ERROR](state, error) {
    state.errors = error;
  },
  [SET_AUTH](state, user) {
    state.isAuthenticated = true;
    state.user = user;
    state.errors = {};
    JwtService.saveToken(state.user.token);
  },
  [SET_PASSWORD](state, password) {
    state.user.password = password;
  },
  [PURGE_AUTH](state) {
    state.isAuthenticated = false;
    state.user = {};
    state.errors = {};
    JwtService.destroyToken();
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};
