import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ApiService from '@/core/api.services'

import CoreuiVue from '@coreui/vue'
import CIcon from '@coreui/icons-vue'
import { iconsSet as icons } from '@/assets/icons'
import DocsExample from '@/components/DocsExample'
import jwtService from './core/jwt.service'

const app = createApp(App)
app.use(store)
app.use(router)
app.use(CoreuiVue)
app.provide('icons', icons)
app.component('CIcon', CIcon)
app.component('DocsExample', DocsExample)
ApiService.init();

if (jwtService.getToken()) {
    ApiService.setHeader();
}else{
    router.push({ name: "Login" });
}

app.mount('#app')
